# Failover Test Plan

## README

If you are volunteering, please make yourself available to join the **GCP Migration Rehearsal** call. Ping @andrewn or @meks for an invite.

* Zoom link: https://gitlab.zoom.us/j/859814316
* Slack channel: [#gcp_migration](https://gitlab.slack.com/messages/C7S4KUEPN)

## High-level Overview

This test plan requires some manual testing as well as some automated testing using [GitLab QA].

The automated and manual QA processes can be conducted in parallel with one another.

The plan will also involve testing during two distinct phases of the failover plan:

- **During the Blackout** (inside the maintenance window period):
  - All the tests conducted during this period are related to features critical to the operation of GitLab.com.
  - **The maintenance window cannot be closed until these tests pass.**
- **After the Blackout** (after the maintenance window period):
  - These tests are non-critical, but, in order to keep the maintenance window as short as possible, should be conducted after the new failover GitLab instance is public.

## Automated QA

### During the Blackout

@meks / @remy to run [GitLab QA] against failed-over environment.

**Important:** Until https://gitlab.com/gitlab-org/gitlab-qa/issues/258 is fixed, we will not be showing gitlab-qa console logs in the recording.

- [ ] Make sure to export the following environment variables
  ```
  export GITLAB_USERNAME=gitlab-qa GITLAB_PASSWORD=xxx GITLAB_QA_ACCESS_TOKEN=xxx
  ```
- [ ] Automated QA complete with 100% pass

  ```
  gitlab-qa Test::Instance::Staging
  ```

  ```
  Put the results of Gitlab QA run here
  ```

## Manual QA

------------

PLEASE FOLLOW THESE INSTRUCTIONS AND REMOVE THEM ONCE DONE.

@meks / @remy to follow the following steps:

1. Open https://docs.google.com/spreadsheets/d/15AtBb6s2p_HvtUe5G9GUSc2ngt69X8dO-418zMuT4us/edit#gid=0
1. Duplicate the document and call it `GCP Migration Manual Testing - yyyy:mm:dd`
1. Set the permissions so that **everyone at GitLab can edit**.
1. Replace `LINK_TO_MANUAL_TESTPLAN` below with a link to the test plan here.

------------

**Manual test plan:** `LINK_TO_MANUAL_TESTPLAN`

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa

/label ~"Failover Execution"
